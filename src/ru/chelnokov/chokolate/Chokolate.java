package ru.chelnokov.chokolate;

import java.util.Scanner;

/**
 * Класс, высчитывающий количество шоколадок, которое можно купить
 * за введённое количество денег в связи с акцией
 *
 * @author Chelnokov E.I. 16IT18K
 */

public class Chokolate {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Деньги: ");
        int cash = reader.nextInt();
        System.out.println("Цена шоколадки: ");
        int price = reader.nextInt();
        System.out.println("Количество обёрток для бонуса: ");
        int plusOne = reader.nextInt();
        findMax(cash, price, plusOne);
    }

    /**
     * Находит количество полученных шоколадок
     *
     * @param cash количество денег, которые вы имеете
     * @param price цена одной шоколадки
     * @param plusOne количество обёрток, при которых вы получите бонусную шоколадку
     */
    private static void findMax(int cash, int price, int plusOne) {
        if (price > cash){
            System.out.println("У вас недостаточно денег(");
        }else if (plusOne == 1) {
            System.out.println("Такой акции не существует(");
        } else {
            int chokolates = cash / price;
            int wrap = chokolates;
            while (wrap >= plusOne) {
                chokolates = chokolates + wrap / plusOne;
                wrap = wrap / plusOne + wrap % plusOne;
            }
            System.out.println(chokolates);
        }
    }
}