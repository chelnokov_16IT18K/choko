package ru.chelnokov.chokolate;

/**
 * Класс, демонстрирующий перевод чисел любого типа в двоичный код
 */

public class ToBinaryNumbers {
    public static void main(String[] args) {
        int i = 389;//целое положительное
        intToBits(i);

        int n = -386;//целое отрицательное
        intToBits(n);

        double d = 75.38;//вещественное число
        longToBits(d, "Число: %5.2f\n" );


        double pi = Math.PI;
        longToBits(pi,"Число: %10.15f\n");

        double e = 5.0/7;//потеря точности
        System.out.format("Число: %10.16f\n", e);

        e += 300000;
        System.out.format("Число: %10.16f\n", e);
    }

    private static void longToBits(double number, String s) {
        String sResult;
        long numberBits = Double.doubleToLongBits(number);

        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format(s, number);
        System.out.println("Формат чисел с плавающей точкой:");
        System.out.println(number > 0 ? "0" + sResult : sResult);
    }

    private static void intToBits(int number) {
        String intBits = Integer.toBinaryString(number);
        System.out.println("Разряды числа: " + intBits);
    }
}
